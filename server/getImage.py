from os import listdir
import sys
import numpy as np
import cv2
import math
from random import random

videos = list(filter(lambda file: (file.find('.mp4') >= 0), listdir('server/videos')))

sys.stdout.flush()

if(len(sys.argv) == 3):
    videoNumber = int(sys.argv[1])
    frameNumber = int(sys.argv[2])

    # TODO: occasionally frameNumber is 0 and that crashes us
    video = 'server/videos/' + videos[videoNumber]
    cap = cv2.VideoCapture(video)
    cap.set(1, frameNumber)
    ret, frame = cap.read()
    grayFrame = cv2.resize(cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY), (320, 180))
    cv2.imwrite('server/temp/video-' + str(videoNumber) + '-frame-' + str(frameNumber) + '-color.jpg', frame)
    cv2.imwrite('server/temp/video-' + str(videoNumber) + '-frame-' + str(frameNumber) + '-gray.jpg', grayFrame)
    sys.stdout.write('done')

else:
    videoNumber = math.floor(random() * len(videos))
    video = 'server/videos/' + videos[videoNumber]
    cap = cv2.VideoCapture(video)
    frameNumber = math.floor(random() * int(cap.get(cv2.CAP_PROP_FRAME_COUNT)))
    sys.stdout.write(str(videoNumber) + ',' + str(frameNumber))


cap.release()
exit(0)
