const { spawn } = require('child_process');

const retrain = () => {
  const train = spawn('python3', ['server/retrain.py',
    '--bottleneck_dir=server/bottlenecks',
    '--model_dir=server/inception',
    '--output_labels=server/retrained_labels.txt',
    '--output_graph=server/retrained_graph.pb',
    '--image_dir=server/images/']);
  train.stdout.on('data', (data) => {
    console.log(data.toString('utf8'));
  });

  train.stderr.on('data', (data) => {
    console.log(data.toString('utf8'));
  });

  train.on('close', (code) => {
    console.log('done', code);
  });
};

retrain();
