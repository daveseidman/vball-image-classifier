const fs = require('fs');
const path = require('path');
const { spawn } = require('child_process');
const express = require('express');

const app = express();

// prevent caching otherwise server would never re-generate images
// that have already been loaded
app.use((req, res, next) => {
  res.set('Cache-Control', 'no-store, no-cache, must-revalidate, private');
  next();
});


// get a random video and frame number
app.get('/api/requestFrame/', (req, res) => {
  const getFrame = spawn('python3', ['server/getImage.py']);
  getFrame.stdout.on('data', (data) => {
    const videoNumber = data.toString('utf8').split(',')[0];
    const frameNumber = data.toString('utf8').split(',')[1];
    res.send({ videoNumber, frameNumber });
  });
});

// create a color and grayscale version of image
app.get('/api/images/:video/:frame', (req, res) => {
  const { video, frame } = req.params;
  console.log(video, frame);
  const getImage = spawn('python3', ['server/getImage.py', video, frame]);
  getImage.stdout.on('data', (data) => {
    if (data.toString('utf8') === 'done') {
      res.sendFile(path.join(__dirname, `temp/video-${req.params.video}-frame-${req.params.frame}-color.jpg`));
    }
  });
});


app.get('/api/classifyImage/:video/:frame/:type', (req, res) => {
  const { video, frame, type } = req.params;
  const colorImage = path.join(__dirname, `temp/video-${video}-frame-${frame}-color.jpg`);
  const grayImage = path.join(__dirname, `temp/video-${video}-frame-${frame}-gray.jpg`);

  // put the image in the specified type folder
  if (type !== 'undefined') {
    fs.rename(grayImage, path.join(__dirname, `images/${type}/video-${video}-frame-${frame}-gray.jpg`), () => {
      fs.unlink(colorImage, () => {
        res.send({ status: 'ok' });
      });
    });
  }
  // no type was specified, delte this frame
  else {
    fs.unlink(colorImage, () => {
      fs.unlink(grayImage, () => {
        res.send({ status: 'ok' });
      });
    });
  }
});

app.get('/api/deleteImage/:video/:frame', (req, res) => {
  const { video, frame } = req.params;
  const colorImage = path.join(__dirname, `temp/video-${video}-frame-${frame}-color.jpg`);
  const grayImage = path.join(__dirname, `temp/video-${video}-frame-${frame}-gray.jpg`);
  fs.unlink(colorImage, () => {
    fs.unlink(grayImage, () => {
      res.send({ status: 'ok' });
    });
  });
});

app.get('/api/resetImage/:video/:frame/:type', (req, res) => {
  const { video, frame, type } = req.params;
  const grayImage = path.join(__dirname, `images/${type}/video-${video}-frame-${frame}-gray.jpg`);
  fs.unlink(grayImage, () => {
    res.send({ status: 'ok' });
  });
});


app.get('/api/undo/:type/:video/:frame', (req, res) => {
  console.log(req.params);
  res.send({ status: 'ok' });
});


app.get('/api/labelImage/:video/:frame', (req, res) => {
  const { video, frame } = req.params;
  const label = spawn('python3', ['server/labelOne.py', `server/temp/video-${video}-frame-${frame}-gray.jpg`]);

  label.stdout.on('data', (data) => {
    // const arr = data.toString('utf8').split(os.EOL);
    // arr.pop();
    console.log(JSON.parse(data.toString('utf8')));
    // res.send(data.toString('utf8'));

    res.send(JSON.parse(data.toString('utf8')));
  });
});

app.listen(8000, () => { console.log('server listening on port 8000'); });


// TODO: decide if this should be available to client:
// const retrain = () => {
//   const train = spawn('python3', ['server/retrain.py',
//     '--bottleneck_dir=server/bottlenecks',
//     '--model_dir=server/inception',
//     '--output_labels=server/retrained_labels.txt',
//     '--output_graph=server/retrained_graph.pb',
//     '--image_dir=server/images/']);
//   train.stdout.on('data', (data) => {
//     console.log(data.toString('utf8'));
//   });
//
//   train.stderr.on('data', (data) => {
//     console.log(data.toString('utf8'));
//   });
//
//   train.on('close', (code) => {
//     console.log('done', code);
//   });
// };
//
