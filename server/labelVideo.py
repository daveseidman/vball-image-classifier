import sys
import numpy as np
import cv2
import math
from random import random
import tensorflow as tf

video = 'server/videos/test-01.mp4'   # 5:45

labels = ['idle', 'playing']

interval = 15

with tf.compat.v1.gfile.FastGFile('server/retrained_graph.pb', 'rb') as f:
    graph_def = tf.compat.v1.GraphDef()
    graph_def.ParseFromString(f.read())
    tf.import_graph_def(graph_def, name='')


cap = cv2.VideoCapture(video)
duration = cap.get(cv2.CAP_PROP_FRAME_COUNT)
fps = cap.get(cv2.CAP_PROP_FPS)

current = 0

while current < duration and current < 500:
    cap.set(1, current)
    ret, frame = cap.read()
    grayFrame = cv2.resize(cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY), (320, 180))
    filename = 'server/temp/frame-' + str(current) + '.jpg'
    cv2.imwrite(filename, grayFrame)

    image_data = tf.compat.v1.gfile.FastGFile(filename, 'rb').read()

    with tf.compat.v1.Session() as sess:
        softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')
        predictions, = sess.run(softmax_tensor, {'DecodeJpeg/contents:0': image_data})

    print('{ "frame": ', current, ', "time": ', current * fps, ', "', labels[1], '": ', predictions[1], ' }', sep='')
    current += interval

cap.release()
exit(0)
