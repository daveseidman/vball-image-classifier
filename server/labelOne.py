import sys
import tensorflow as tf

print(len(tf.config.experimental.list_physical_devices('GPU')))

labels = ['idle', 'playing']

with tf.compat.v1.gfile.FastGFile('server/retrained_graph.pb', 'rb') as f:
    graph_def = tf.compat.v1.GraphDef()
    graph_def.ParseFromString(f.read())
    tf.import_graph_def(graph_def, name='')

image_data = tf.compat.v1.gfile.FastGFile(sys.argv[1], 'rb').read()

with tf.compat.v1.Session() as sess:
   softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')
   predictions, = sess.run(softmax_tensor, {'DecodeJpeg/contents:0': image_data})

   print('{ "' + labels[0] + '" : ' + str(predictions[0]) + ', "' + labels[1] + '" : ' + str(predictions[1]) + ' }')
