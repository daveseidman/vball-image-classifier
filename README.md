# AI Volleyball image classifier

The hope of this project is to train and deploy an AI model to analyze video from a volleyball session and determine the timecodes for when each volley begins and ends and then use that data to automatically edit the video to exclude everything that's not part of a volley.

.