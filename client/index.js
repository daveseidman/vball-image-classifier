import './index.scss';

class VballClassifier {
  constructor() {
    this.loaded = this.loaded.bind(this);
    this.getImage = this.getImage.bind(this);

    this.image = document.createElement('img');
    this.image.className = 'image loading';
    this.image.addEventListener('load', this.loaded);

    this.prediction = document.createElement('div');
    this.prediction.className = 'prediction';

    document.body.appendChild(this.image);
    document.body.appendChild(this.prediction);

    this.history = [];

    this.listen();
    this.getImage();
  }

  listen() {
    window.addEventListener('keydown', ({ key }) => {
      if (!(this.videoNumber && this.frameNumber)) return;

      if (key.toLowerCase() === 'i') this.classify('idle');
      if (key.toLowerCase() === 'p') this.classify('playing');
      if (key.toLowerCase() === 'o') this.classify(); // {
      // this.type = null;
      // this.videoNumberPrev = this.videoNumber;
      // this.frameNumberPrev = this.frameNumber;
      // this.delete(this.type, this.videoNumber, this.frameNumber).then(this.getImage);
      // }

      if (key.toLowerCase() === 't') this.test();
      if (key.toLowerCase() === 'z') this.undo();
    });

    // window.onbeforeunload = () => { this.delete(null, this.videoNumber, this.frameNumber); };
    window.onbeforeunload = () => { this.classify(); };
  }

  getImage() {
    fetch('/api/requestFrame').then(res => res.json()).then((res) => {
      this.videoNumber = res.videoNumber;
      this.frameNumber = res.frameNumber;
      this.loadImage();
    });
  }

  loadImage() {
    this.image.src = `api/images/${this.videoNumber}/${this.frameNumber}`;
  }

  classify(type) {
    this.history.push({ videoNumber: this.videoNumber, frameNumber: this.frameNumber, type });
    this.image.classList.add('loading');
    fetch(`/api/classifyImage/${this.videoNumber}/${this.frameNumber}/${type}`).then(res => res.json()).then(() => {
      this.videoNumber = null;
      this.frameNumber = null;
      this.getImage();
    });
  }

  undo() {
    if (!this.history.length) return;
    this.image.classList.add('loading');
    // first dump the current image we're looking at since we're not classifying it
    fetch(`/api/deleteImage/${this.videoNumber}/${this.frameNumber}`).then(res => res.json()).then(() => {
      const { videoNumber, frameNumber, type } = this.history.pop();
      this.videoNumber = videoNumber;
      this.frameNumber = frameNumber;

      // if it was classified, pull it back out of that folder
      fetch(`/api/resetImage/${videoNumber}/${frameNumber}/${type}`).then(res => res.json()).then(this.loadImage());
    });
  }

  test() {
    this.prediction.innerText = 'Analyzing Frame...';
    this.image.classList.add('loading');
    fetch(`api/labelImage/${this.videoNumber}/${this.frameNumber}`).then(res => res.json()).then((res) => {
      this.image.classList.remove('loading');
      console.log(res);
      this.prediction.innerText = res;
      this.prediction.classList.remove('hiddenn');
      setTimeout(() => { this.prediction.classList.add('hidden'); }, 20000);
    });
  }

  loaded() {
    this.image.classList.remove('loading');
  }
}

const classifier = new VballClassifier(); // eslint-disable-line
